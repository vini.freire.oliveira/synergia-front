import React, { Component, Fragment } from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Auth from './pages/Auth';
import Dashboard from './pages/Dashboard';
import DashboardAdmin from "./pages/DashboardAdmin";
class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <Fragment>
                <Switch>
                    <Route path="/auth" component={Auth}/>
                    <Route path="/dashboard" component={Dashboard}/>
                    <Route path="/admin" component={DashboardAdmin}/>
                </Switch>
            </Fragment>
        </BrowserRouter>
    );
  }
}

export default App;
