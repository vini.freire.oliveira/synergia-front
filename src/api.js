import axios from 'axios';
import {BASE_URL} from './helpers/session';

const API = axios.create({
    // baseURL: process.env.RAILS_API
    baseURL: BASE_URL
    // baseURL: "http://localhost:3000/"
});

export default API;
