import { combineReducers } from "redux";
// import the create reducers
import CurrentUserReducer from './pages/Dashboard/reducer';
import CurriculumsReducer from './containers/DatatableCurriculumContainer/reducer';

const rootReducer = combineReducers({
    current_user: CurrentUserReducer,
    curriculums: CurriculumsReducer,
});

export default rootReducer;
