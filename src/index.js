import React, { Fragment} from 'react';
import ReactDOM from 'react-dom';

import Global from './styles/settings/';
import Reset from './styles/generic';
import Base from './styles/generic';

import App from './App';

import { Provider } from 'react-redux';
import Store, { history } from './configureStore';
import { ConnectedRouter } from 'react-router-redux'

ReactDOM.render(
            <Fragment>
                <Global.Colors/>
                <Global.Size/>
                <Base/>
                <Reset/>
                <Provider store={Store}>
                    <App />
                </Provider>
            </Fragment>
    , document.getElementById('root'));
