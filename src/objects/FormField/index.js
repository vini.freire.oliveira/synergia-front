import styled from 'styled-components';

const FixedButton = styled.button`
  position: fixed;
  top: 0;
  right: 10px;
  padding-bottom: 10px;
  margin-top: 10px;
  color: white;
`;

const FormCenter = styled.div`
  margin-bottom: 100px;
`;

const FormTitle = styled.div`
  color: #707C8B;
  font-weight: 300;
  margin-bottom: 50px;
`;

const FormFields = styled.form`
`;

const FormLogin = styled.div`
   div{
    margin-bottom: 40px;
   }
   
   label{
    font-size: .9em;
   }
`;

const FormField = styled.div`
  margin-bottom: 10px;
`;

const FormButtonField = styled.div`
  margin-bottom: 10px;
  @media(max-width: 700px){
    display: inline;
  }
  
`;

const FormFieldLabel = styled.label`
  display: block;
  text-transform: uppercase;
  font-size: .8em;
  ${props =>
    props.white ? `
        color: white;      
    ` : `
        color: #838383;
        `
    };

  
`;

const FormFieldInput = styled.input`
  width: 85%;
  background-color: transparent;
  border: none;
  ${props =>
    props.white ? `

        color: white;      
    ` : `
        color: black;
        `
    };
  
  outline: none;
  border-bottom: 1px solid #FBAB7E;
  font-size: 1em;
  font-weight: 300;
  padding-bottom: 10px;
  margin-top: 10px;
  &::placeholder{
    color: #616E7F
  }
`;

const FormFieldButton = styled.button`
  border: none;
  color: white;
  outline: none;
  border-radius: 25px;
  padding: 10px 70px;
  font-size: .9em;
  font-weight: 500;
  margin-right: 20px;
  @media(max-width: 700px){
    width: 100%;
  }
  ${props =>
    props.red ? `
        background-color: #ff0000;
    ` : `
        background-color: #dba000;
        `
    };
`;

const UploadButton = styled.input`
  margin-top: 10px;
  background-color: #dba000;
  color: white;
  border: none;
  outline: none;
  border-radius: 25px;
  padding: 10px 70px;
  font-size: .9em;
  font-weight: 500;
  margin-right: 20px;
  @media(max-width: 700px){
    width: 100%;
  }
`;

const FormFieldCheckbox = styled.input`
    position: relative;
    top: 1.5px;
`;

const FormFieldTermsLink = styled.a`
    color: white;
    border-bottom: 1px solid #199087;
    text-decoration: none;
    display: inline-block;
    padding-bottom: 2px;
    margin-left: 5px;
`;


export {
    FormCenter,
    FormTitle,
    FormFields,
    FormButtonField,
    FormField,
    FormFieldLabel,
    FormFieldInput,
    FormFieldButton,
    FormFieldCheckbox,
    FormFieldTermsLink,
    FormLogin,
    UploadButton,
    FixedButton
}
