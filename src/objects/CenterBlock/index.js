import styled from 'styled-components';

const CenterBlock = styled.div`
    background: #fff;
    border-radius: 3px;
    -webkit-box-shadow: 0px 0px 22px -1px rgba(0,0,0,0.14);
    -moz-box-shadow: 0px 0px 22px -1px rgba(0,0,0,0.14);
    box-shadow: 0px 0px 22px -1px rgba(0,0,0,0.14);
    width: ${props => props._fullWidth ? "100%" : "70%"};
    margin: auto;
    margin-top: 30px;
    padding: 20px 30px;
`;

export default  CenterBlock;
