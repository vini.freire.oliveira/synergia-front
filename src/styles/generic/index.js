import { createGlobalStyle } from "styled-components";

const Reset = createGlobalStyle`
  *{
      margin: 0;
      padding: 0;
      box-sizing: border-box;
  }    
  
  html {
    height: 100%
  }

  body {
    background-color: #FBAB7E;
    background-image: linear-gradient(62deg, #FBAB7E 0%, #F7CE68 100%) no-repeat;
  }
`;

export default Reset;
