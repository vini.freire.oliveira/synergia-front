import React, { Fragment, Component } from 'react';
import { Route, NavLink } from 'react-router-dom';
import SignInContainer from '../../containers/Auth/SignInContainer';
import SignUpContainer from '../../containers/Auth/SignUpContainer';
import CenterBlock from '../../objects/CenterBlock';

import './style.css'
export default class PageSwitcher extends Component{
    render() {
        return(
            <Fragment>
                <CenterBlock _fullWidth>
                    <img src="http://www.synergiaconsultoria.com.br/wp-content/uploads/2016/01/topo-logo-synergia.png?x41292" className="logo"></img>
                     <div className="FormTitle">
                        <NavLink to="/auth/sign-in" activeClassName="FormTitle__Link--Active" className="FormTitle__Link"> Sign In </NavLink>
                        or
                        <NavLink exact to="/auth/sign-up" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">Sign Up</NavLink>
                    </div>
                    <Route exact path="/auth/sign-up" component={SignUpContainer}>
                    </Route>
                    <Route path="/auth/sign-in" component={SignInContainer}>
                    </Route>
                </CenterBlock>
            </Fragment>
        );
    }
}
