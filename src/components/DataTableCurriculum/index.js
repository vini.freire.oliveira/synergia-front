import React, {Component, Fragment} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import PersonCard from '../PersonCard';
import {FixedButton, FormCenter} from "../../objects/FormField";


export default class DataTableCurriculum extends Component {
    constructor(){
        super()
    }
    onAfterDeleteRow(rowKeys) {
        alert('The rowkey you drop: ' + rowKeys);
    }

    options = {
        afterDeleteRow: this.onAfterDeleteRow  // A hook for after droping rows.
    };

// If you want to enable deleteRow, you must enable row selection also.
    selectRowProp = {
        mode: 'radio'
    };


    logout = event => {
        event.preventDefault();
        this.props.logout()
    };

    render() {
        var curriculums = this.props.curriculums.length ? (this.props.curriculums) : [];
        console.log(curriculums);
        return(
            <Fragment>
            <FixedButton onClick={this.logout}>
                Logout
            </FixedButton>
            {curriculums.map((curriculum, index) =>
                <PersonCard {...curriculum}
                    key={index}
                />
            )}
            </Fragment>
        )
    }
}
