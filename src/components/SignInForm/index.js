import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { LocalForm } from 'react-redux-form';
// import { LocalForm } from 'redux-form';
import {
    FormCenter,
    FormField,
    FormFieldButton,
    FormFieldInput,
    FormFieldLabel,
    FormFields,
    FormButtonField,
    FormLogin
} from '../../objects/FormField';
import './style.css'

export default class SignInForm extends Component {

    constructor(){
        super()
        this.state = {
            user: {
                email: "",
                password: ""
            }
        }
    }

    handleChange = event => {
        const user = { ...this.state.user };
        let target = event.target;
        user[target.name] = target.type === 'checkbox' ? target.checked : target.value;

        this.setState({ user })
    };

    handleSubmit = event => {
        this.props.handleSubmit(this.state.user);
    };

    render() {
        return (
            <FormCenter>
                <LocalForm className="FormFields" onSubmit={this.handleSubmit}>
                    <FormLogin>
                        <FormField>
                            <FormFieldLabel htmlFor="email">E-Mail</FormFieldLabel>
                            <FormFieldInput
                                type="email"
                                id="email"
                                placeholder="Enter your email"
                                name="email"
                                value={this.state.user.email}
                                onChange={this.handleChange}
                            />
                        </FormField>

                        <FormField>
                            <FormFieldLabel htmlFor="password">Senha</FormFieldLabel>
                            <FormFieldInput
                                type="password"
                                id="password"
                                placeholder="Entre Com Sua Senha"
                                name="password"
                                value={this.state.user.password}
                                onChange={ this.handleChange}
                            />
                        </FormField>
                    </FormLogin>
                    <FormField>
                        <FormFieldButton type="submit" >Sign In</FormFieldButton>
                        <Link to="sign-up" className="FormField__Link">Create an account</Link>
                    </FormField>
                </LocalForm>
            </FormCenter>
        );
    }
}
