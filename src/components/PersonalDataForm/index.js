import React, {Component, Fragment} from 'react';
import {Row, Col} from 'react-materialize';

import {makeStyles} from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import {
    FormCenter,
    FormField,
    FormFieldButton,
    FormFieldInput,
    FormFieldLabel,
    FormFields,
    UploadButton,
    FixedButton
} from '../../objects/FormField';
import Card from "../../objects/Card";
import Content from "../../objects/Content";
import Upload from '../Upload';

export default class PersonalDataForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {
                avatar: "",
                curriculum: "",
                birthdate: "25/04/1998",
                marital_status: "single",
                academic: "",
                wage_claim: "",
                phones_attributes: [{
                    type_phone: "",
                    area_code: "",
                    number: ""
                }],
                addresses_attributes: [{
                    street: "",
                    cep: "",
                    number: "",
                    city: "",
                    district: "",
                    state: "",
                    neighborhood: "",
                    complement: ""
                }]
            },
            marital_status_list: ["casado", "solteiro"],
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeContact = this.handleChangeContact.bind(this);
        this.handleChangeAddress = this.handleChangeAddress.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChangeHandler = this.onChangeHandler.bind(this);
    }

    componentDidMount() {
        setTimeout(() => {
            if (this.props.current_user.addresses.length > 0 && this.props.current_user.phones.length > 0) {
            let phone = this.props.current_user.phones.pop();
            let address = this.props.current_user.addresses.pop();

            console.log(this.props.current_user.addresses.length)
            console.log(this.props.current_user.phones.length)
                this.setState({
                    user: {
                        avatar: this.props.current_user.avatar,
                        curriculum: this.props.current_user.curriculum,
                        birthdate: this.props.current_user.birthdate,
                        marital_status: this.props.current_user.marital_status,
                        academic: this.props.current_user.academic,
                        wage_claim: this.props.current_user.wage_claim,
                        phones_attributes: [{
                            type_phone: phone.type_phone,
                            area_code: phone.area_code,
                            number: phone.number
                        }],
                        addresses_attributes: [{
                            street: address.street,
                            cep: address.cep,
                            number: address.number,
                            city: address.city,
                            district: address.district,
                            state: address.state,
                            neighborhood: address.neighborhood,
                            complement: address.complement
                        }]
                    }
                });
            }
        }, 5000);
    }

    handleChange = event => {
        const user = {...this.state.user};
        let target = event.target;
        user[target.name] = target.type === 'checkbox' ? target.checked : target.value;

        this.setState({user});
    };

    handleChangeContact = event => {
        const user = {...this.state.user};
        let target = event.target;
        user.phones_attributes[0][target.name] = target.value;
        this.setState({user})
    };

    handleChangeAddress = event => {
        const user = {...this.state.user};
        let target = event.target;
        user.addresses_attributes[0][target.name] = target.value;
        this.setState({user})
    };

    onChangeHandler = event => {
        const user = this.state.user;
        if(event.target.files[0].type === "application/pdf"){
            user.curriculum = event.target.files[0];
        }else{
            user.avatar = event.target.files[0];
        }
        console.log(event.target.files);

        this.setState({user});
    };

    uploadCurriculum = event => {
        const data = new FormData();
        data.append('user[curriculum]', this.state.user.curriculum);
        this.props.uploadCurriculum(data);
    };

    uploadImg = event => {
        const data = new FormData();
        data.append('user[avatar]', this.state.user.avatar);
        this.props.uploadCurriculum(data);
    };

    handleSubmit = event => {
        event.preventDefault();
        console.log(this.state.user);
        this.props.handleSubmit(this.state.user);
    };

    deleteMyAccount = event => {
        event.preventDefault();
        this.props.deleteMyAccount()
    };

    logout = event => {
        event.preventDefault();
        this.props.logout()
    };


    render() {
        return (
            <FormCenter>
                <FixedButton onClick={this.logout}>
                    Logout
                </FixedButton>
                <FormFields onSubmit={this.handleSubmit}>
                    <h5>Informações Pessoais</h5>
                    <Row>
                        <Col s={6}>
                            <FormField>
                                <FormFieldLabel htmlFor="last_name">Nome</FormFieldLabel>
                                <FormFieldInput
                                    value={this.props.current_user.name}
                                    disabled
                                />
                            </FormField> {/* Pretenção Salarial */}
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6}>
                            <FormField>
                                <FormFieldLabel htmlFor="last_name">Pretenção Salarial</FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="wage_claim"
                                    placeholder="Pretenção Salarial"
                                    name="wage_claim"
                                    value={this.state.user.wage_claim}
                                    onChange={this.handleChange}
                                />
                            </FormField> {/* Pretenção Salarial */}
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6}>
                            <FormControl>
                                <FormFieldLabel htmlFor="marital_status">Formação</FormFieldLabel>
                                <Select
                                    value={this.state.user.academic}
                                    onChange={this.handleChange}
                                    input={<Input name="academic" id="academic"/>}
                                    displayEmpty
                                    name="academic"
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    <MenuItem value={"medium"}>Medio</MenuItem>
                                    <MenuItem value={"postgraduate"}>pos-graduação</MenuItem>
                                    <MenuItem value={"graduate"}>graduação</MenuItem>
                                </Select>
                            </FormControl>
                        </Col>
                        <Col s={4}>
                            <FormField>
                                <FormFieldLabel htmlFor="marital_status">BirthDate</FormFieldLabel>
                                <FormFieldInput
                                    type="date"
                                    id="birthdate"
                                    placeholder="dd/mm/aaaa"
                                    value={this.state.user.birthdate}
                                    name="birthdate"
                                    onChange={this.handleChange}
                                />
                            </FormField>
                        </Col>
                        <Col s={3}>
                            <FormFieldLabel htmlFor="marital_status">Estado Civil</FormFieldLabel>
                            <FormControl>
                                <Select
                                    value={this.state.user.marital_status}
                                    onChange={this.handleChange}
                                    input={<Input name="marital_status" id="marital_status"/>}
                                    displayEmpty
                                    name="marital_status"
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    <MenuItem value={"single"}>Solteiro</MenuItem>
                                    <MenuItem value={"married"}>Casado</MenuItem>
                                    <MenuItem value={"widower"}>Viuvo</MenuItem>
                                </Select>
                            </FormControl>
                        </Col>
                    </Row>

                    <h5>Informações De Contato</h5>
                    <Row>
                        <Col s={3}>
                            <FormControl>
                                <FormFieldLabel htmlFor="marital_status">Tipo de Telefone</FormFieldLabel>
                                <Select
                                    value={this.state.user.phones_attributes[0].type_phone}
                                    onChange={this.handleChangeContact}
                                    input={<Input name="type_phone" id="type_phone"/>}
                                    displayEmpty
                                    name="type_phone"
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    <MenuItem value={"phone"}>Telefone</MenuItem>
                                    <MenuItem value={"cellphone"}>Celular</MenuItem>
                                    <MenuItem value={"fax"}>FAX</MenuItem>
                                </Select>
                            </FormControl>
                        </Col>
                        <Col s={3}>
                            <FormField>
                                <FormFieldLabel htmlFor="marital_status">DDD</FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="area_code"
                                    placeholder="DDD"
                                    name="area_code"
                                    value={this.state.user.phones_attributes[this.state.user.phones_attributes.length - 1].area_code}
                                    onChange={this.handleChangeContact}
                                />
                            </FormField>{/* DDD */}
                        </Col>
                        <Col s={3}>
                            <FormField>
                                <FormFieldLabel htmlFor="marital_status">number</FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="number"
                                    placeholder="999999999"
                                    name="number"
                                    value={this.state.user.phones_attributes[0].number}
                                    onChange={this.handleChangeContact}
                                />
                            </FormField>{/* DDD */}
                        </Col>
                    </Row>
                    <h5>Address Data</h5>
                    <Row>
                        <Col s={6}>
                            <FormField>
                                <FormFieldLabel htmlFor="street">Rua</FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="street"
                                    placeholder="Rua"
                                    name="street"
                                    value={this.state.user.addresses_attributes[0].street}
                                    onChange={this.handleChangeAddress}
                                />
                            </FormField> {/* Rua */}
                        </Col>
                        <Col s={6}>
                            <FormField>
                                <FormFieldLabel htmlFor="last_name">CEP </FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="cep"
                                    placeholder=" CEP "
                                    name="cep"
                                    value={this.state.user.addresses_attributes[0].cep}
                                    onChange={this.handleChangeAddress}
                                />
                            </FormField> {/* CEP */}
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6}>
                            <FormField>
                                <FormFieldLabel htmlFor="marital_status">Numero</FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="number"
                                    placeholder="Numero"
                                    name="number"
                                    value={this.state.user.addresses_attributes[0].number}
                                    onChange={this.handleChangeAddress}
                                />
                            </FormField>{/* Numero */}
                        </Col>
                        <Col s={6}>
                            <FormField>
                                <FormFieldLabel htmlFor="city">Cidade</FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="city"
                                    placeholder="Cidade"
                                    name="city"
                                    value={this.state.user.addresses_attributes[0].city}
                                    onChange={this.handleChangeAddress}
                                />
                            </FormField>{/* Cidade */}
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6}>
                            <FormField>
                                <FormFieldLabel htmlFor="marital_status">Distrito</FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="district"
                                    placeholder="Distrito"
                                    name="district"
                                    value={this.state.user.addresses_attributes[0].district}
                                    onChange={this.handleChangeAddress}
                                />
                            </FormField>{/* Distrito */}
                        </Col>
                        <Col s={6}>
                            <FormField>
                                <FormFieldLabel htmlFor="marital_status">Estado</FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="state"
                                    placeholder="Estado"
                                    name="state"
                                    value={this.state.user.addresses_attributes[0].state}
                                    onChange={this.handleChangeAddress}
                                />
                            </FormField>{/* Estado */}
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6}>
                            <FormField>
                                <FormFieldLabel htmlFor="marital_status">Bairro</FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="neighborhood"
                                    placeholder="Bairro"
                                    name="neighborhood"
                                    value={this.state.user.addresses_attributes[this.state.user.addresses_attributes.length - 1].neighborhood}
                                    onChange={this.handleChangeAddress}
                                />
                            </FormField>{/* Bairro */}
                        </Col>
                        <Col s={6}>
                            <FormField>
                                <FormFieldLabel htmlFor="marital_status">Complemento</FormFieldLabel>
                                <FormFieldInput
                                    type="text"
                                    id="complement"
                                    placeholder="Complemento"
                                    name="complement"
                                    value={this.state.user.addresses_attributes[0].complement}
                                    onChange={this.handleChangeAddress}
                                />
                            </FormField>{/* Estado */}
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6}>
                            <form>
                                <FormFieldLabel htmlFor="last_name">Avatar </FormFieldLabel>
                                <FormFieldInput type="file" name="file_avatar" onChange={this.onChangeHandler}/>
                                <UploadButton type="button" value={"upload"} onClick={this.uploadImg}/>
                            </form>
                        </Col>
                        <Col s={6}>
                            <FormFieldLabel htmlFor="last_name">Curriculo</FormFieldLabel>
                            <FormFieldInput type="file" name="file_curriculum" onChange={this.onChangeHandler}/>
                            <UploadButton type="button" value={"upload"} onClick={this.uploadCurriculum}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                    <FormField>
                        <FormFieldButton white> Atualizar Curriculo </FormFieldButton>
                    </FormField>
                        </Col>
                        <Col>
                            <FormField>
                                <FormFieldButton red onClick={this.deleteMyAccount}> Deletar Meu
                                    Curriculo</FormFieldButton>
                            </FormField>
                        </Col>
                    </Row>
                </FormFields>
            </FormCenter>
        );
    }
}
