import React, { Component } from 'react';
import {
    FormCenter,
    FormTitle,
    FormField,
    FormFields,
    FormFieldLabel,
    FormFieldInput,
    FormFieldButton,
    FormFieldCheckbox,
    FormFieldTermsLink
} from '../../objects/FormField';


export default class SignUpForm extends Component {
    constructor() {
        super();

        this.state = {
            user: {
                email: '',
                password: '',
                password_confirmation: '',
                name: '',
                hasAgreed: false
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = event => {
        const user = { ...this.state.user };
        let target = event.target;
        user[target.name] = target.type === 'checkbox' ? target.checked : target.value;

        this.setState({ user })
    };

    handleSubmit = event => {
        event.preventDefault();
        this.props.handleSubmit(this.state.user);
    };

    render() {
        return(
            <FormCenter>
                <FormFields onSubmit={this.handleSubmit} >
                    <FormField>
                        <FormFieldLabel htmlFor="name"> Full Name </FormFieldLabel>
                        <FormFieldInput
                            type="text"
                            id="name"
                            placeholder="Entre com Seu nome"
                            name="name"
                            value={this.state.user.name}
                            onChange={this.handleChange}/>
                    </FormField>
                    <FormField>
                        <FormFieldLabel htmlFor="email"> Email </FormFieldLabel>
                        <FormFieldInput
                            type="email"
                            id="email"
                            placeholder="Entre Com Seu Email"
                            name="email"
                            value={this.state.user.email}
                            onChange={this.handleChange}/>
                    </FormField>
                    <FormField>
                        <FormFieldLabel htmlFor="password"> Password </FormFieldLabel>
                        <FormFieldInput
                            type="password"
                            id="password"
                            placeholder="Entre Com Sua Senha"
                            name="password"
                            value={this.state.user.password}
                            onChange={this.handleChange}/>
                    </FormField>

                    <FormField>
                        <FormFieldLabel htmlFor="password_confirmation"> Password Confirmation </FormFieldLabel>
                        <FormFieldInput
                            type="password"
                            id="password_confirmation"
                            placeholder="Entre Com Sua Senha"
                            name="password_confirmation"
                            value={this.state.user.password_confirmation}
                            onChange={this.handleChange}/>
                    </FormField>
                    <FormField>
                        <FormFieldButton >Sign Up </FormFieldButton>
                    </FormField>
                </FormFields>
            </FormCenter>
        )
    }
}
