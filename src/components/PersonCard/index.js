import React from "react";
import { Row, Col, Card, Icon, Dropdown, NavItem } from 'react-materialize';
import Avatar from '../../objects/Avatar';
import fake_avatar from '../../imgs/fake_avatar.png';
import {BASE_URL} from '../../helpers/session';

const PersonCard = (props) => {
    // let avatar;
    // if(props.avatar){
    //     avatar = "http://172.18.9.122:3000" + props.avatar
    // }else{
    //     avatar = fake_avatar;
    // }
    // let img =
    let phone;
    if( props.phones.length > 0){
        phone = props.phones.pop();
        phone = `(${phone.area_code}) ${phone.number}`;
    }
    let location;
    if (props.addresses.length > 0){
        location = props.addresses.pop();
        location = `${location.street}, ${location.number} - ${location.city}/ ${location.state}, CEP - ${location.cep}`;
    }
    return(
        <Card>
            <Row>
                <Col s={6} m={2} offset="s3">
                    <Avatar src={(props.avatar) ? BASE_URL + props.avatar : fake_avatar} className="responsive-img circle"/>
                </Col>
                <Col s={12} m={10}>
                    <Row>
                        <Col s={6} m={6}>
                            <b>{props.name}</b>
                        </Col>
                        <Col s={6} m={6}>
                            <b>{props.email}</b>
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6} m={6}>
                            <b>{phone}</b>
                        </Col>

                        <Col s={6} m={6}>
                            <b>{`Data de Nascimento :${props.birthdate}` }</b>
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6} m={6}>
                            <b>{`Formação :${props.academic}`}</b>
                        </Col>

                        <Col s={6} m={6}>
                            <b>{`R$ ${props.wage_claim}`}</b>
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6} m={6}>
                            <b>{`Estado Civil :${props.marital_status}`}</b>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <b>{`${location}`}</b>
                        </Col>
                    </Row>
                    <Row>
                        <Col s={10} m={10}>
                            <a href={BASE_URL + props.curriculum} target={"_blank"}>pdf</a>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Card>
    )
}

export default PersonCard;
