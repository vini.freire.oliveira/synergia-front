import React, {Component} from 'react';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import {TableHeaderColumn} from "react-bootstrap-table";


export default class DataTableCurriculum extends Component {
    constructor(){
        super()
        this.state = {
            products: [{
                id: 1,
                name: "Product1",
                price: 120
            }, {
                id: 2,
                name: "Product2",
                price: 80
            }],
            columns:  [{
                dataField: 'id',
                text: 'Product ID'
            }, {
                dataField: 'name',
                text: 'Product Name'
            }, {
                dataField: 'price',
                text: 'Product Price'
            }]
        }
    }

    render() {
        return(
            <BootstrapTable keyField='id' data={ this.products } columns={ this.columns } />
        )
    }
}

