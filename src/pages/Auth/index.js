import React, { Component } from 'react';
import { HashRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import Login from './Login';
import Register from './Register';

export default class Auth extends Component{
    render() {
        return(
            <div className="Login">
                <Route exact path="/auth/sign-up" component={Register}>
                </Route>
                <Route exact path="/auth/sign-in" component={Login}>
                </Route>
            </div>
        );
    }
}
