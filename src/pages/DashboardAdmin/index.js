import React, { Component } from 'react';
import CurriculumContainer from "../../containers/CurriculumContainer";
import CenterBlock from '../../objects/CenterBlock';

export default class DashboardAdmin extends Component{
    render() {
        return(
            <CenterBlock>
                <CurriculumContainer/>
            </CenterBlock>
        );
    }
}
