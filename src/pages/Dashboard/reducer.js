import { FETCH_USER } from './constants';

const initialState = { current_user: [] };

export default function(state = initialState, action) {
    if (action.type === FETCH_USER) {
        return action.payload;
    } else {
        return state;
    }
}
