import API from '../../api';
import {getHeaders, setHeaders} from '../../helpers/session';
import {FETCH_USER} from "./constants";

export function getCurriculum()
{
    const headers = getHeaders();
    console.log(headers);
    const request = API.get('/api/v1/user/1', { headers: headers});

    return (dispatch) => {
        request.then(
            (resp) => {
                console.log(resp.data);
                setHeaders(resp.headers);
                dispatch({ type: FETCH_USER, payload: resp.data })
            },
            error => window.Materialize.toast('', 4000, 'red')
        )
    };
}
