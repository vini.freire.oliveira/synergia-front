import React, { Component } from 'react';
import PersonalDataContainer from '../../containers/PersonalDataContainer';
import CenterBlock from '../../objects/CenterBlock';
import {getCurriculum} from "./actions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class Dashboard extends Component{
    componentDidMount(){
        this.props.getCurriculum();
    }

    render() {
        return(
            <CenterBlock>
                <PersonalDataContainer />
            </CenterBlock>
        );
    }
}

function mapStateToProps(state) {
    return {
    }
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getCurriculum }, dispatch)
}

export default connect(mapStateToProps , mapDispatchToProps)(Dashboard)

