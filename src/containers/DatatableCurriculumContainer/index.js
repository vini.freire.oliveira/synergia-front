import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DataTableCurriculum from '../../components/DataTableCurriculum';
import { logout } from './actions';


class DatatableCurriculumContainer extends React.Component{

    logout = event =>
    {
        this.props.logout()
    }

    render() {
        var curriculums = this.props.curriculums.length ? (this.props.curriculums) : []
        return(
            <DataTableCurriculum
                curriculums={curriculums}
                logout={this.logout}
            />
        )
    }
}

function mapStateToProps(state) {
    return {
        curriculums: state.curriculums
    }
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ logout }, dispatch)
}

export default connect(mapStateToProps , mapDispatchToProps)(DatatableCurriculumContainer)
