import { FETCH_CURRICULUM } from './constants';

const initialState = { curriculums: [] };

export default function(state = initialState, action) {
    if (action.type === FETCH_CURRICULUM) {
        return action.payload;
    } else {
        return state;
    }
}
