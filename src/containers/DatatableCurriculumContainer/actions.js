import API from '../../api';
import {setHeaders, getHeaders, logoutSession} from '../../helpers/session';
import { FETCH_CURRICULUM} from "./constants";

export function getCurriculums()
{
    const headers = getHeaders();
    const request = API.get('/api/v1/user', { headers: headers});

    return (dispatch) => {
        request.then(
            resp => dispatch({ type: FETCH_CURRICULUM, payload: resp.data }),
            error => window.Materialize.toast('Problem in get Timeline', 4000, 'red')
        )
    };
}

export function logout(data){
    const user = {
        user : {data}
    };

    const headers = getHeaders();

    const request = API.delete('/auth/sign_out',  {headers: headers});
    return(dispatch) => {
        request
            .then((response)=>{
                logoutSession();
                window.location.replace("http://localhost:3001/auth/sign-in")
            })
            .catch((error) => {
                window.Materialize.toast('User or password incorrect', 4000, 'red');
                console.log(error)
            })
    }
}
