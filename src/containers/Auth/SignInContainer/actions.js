import API from '../../../api';
import {setHeaders } from '../../../helpers/session';
import { push } from 'react-router-redux'
import { UPDATE_CURRENT_USER, REMOVE_CURRENT_USER } from '../constants';

export function signIn({email, password}) {
    const request = API.post('auth/sign_in', {email, password});

    return(dispatch) =>{
        request
            .then((response)=>{
                console.log(response);
                setHeaders(response.headers);
                dispatch({ type: UPDATE_CURRENT_USER, payload: response.data });
                dispatch(push('/dashboard'));

                if(response.data.data.user_type == "employee"){
                    window.location.replace("http://localhost:3001/dashboard")
                }else{
                    window.location.replace("http://localhost:3001/admin")
                }

            })
            .catch((error)=>{
                window.Materialize.toast('User or password incorrect', 5000, 'red');
                console.log(error)
            })
    }
}
