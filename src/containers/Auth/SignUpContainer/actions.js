import API from '../../../api';
import {setHeaders } from '../../../helpers/session';
import { push } from 'react-router-redux'

export function signUp({name, email, password, password_confirmation}) {
    console.log(name, email, password, password_confirmation);

    const request = API.post('/auth', {name, email, password, password_confirmation});

    return(dispatch) =>{
        request
            .then((response)=>{
                console.log(response.headers);
                setHeaders(response.headers);
                dispatch(push('/dashboard'))
                if(response.data.data.user_type == "employee"){
                    window.location.replace("http://localhost:3001/dashboard")
                }
            })
            .catch((error) => {
                window.Materialize.toast('User or password incorrect', 4000, 'red');
                console.log(error)
                dispatch(push('/dashboard'))
            })
    }
}
