import API from '../../api';
import {setHeaders, getHeaders } from '../../helpers/session';
import { FETCH_CURRICULUM} from "../DatatableCurriculumContainer/constants";

export function getCurriculums()
{
    const headers = getHeaders();
    const request = API.get('/api/v1/user', { headers: headers});

    return (dispatch) => {
        request.then(
            (resp) => {
                console.log(headers);
                setHeaders(resp.headers);
                dispatch({ type: FETCH_CURRICULUM, payload: resp.data })
            },
            error => window.Materialize.toast('Problem in get Timeline', 4000, 'red')
        )
    };
}
