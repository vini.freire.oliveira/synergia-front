import React,{Fragment} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {getCurriculums} from "./actions";
import Dashboard from "../../components/Dashboard";

class CurriculumContainer extends React.Component {
    componentDidMount(){
        this.props.getCurriculums();
    }

    render() {
        return (
            <Fragment>
                <Dashboard/>
            </Fragment>
        )
    }
}
function mapStateToProps(state) {
    return { }
};

function mapDispatchToProps(dispatch){
    return bindActionCreators({getCurriculums}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(CurriculumContainer);
