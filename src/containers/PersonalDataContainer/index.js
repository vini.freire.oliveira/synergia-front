import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PersonalDataForm from '../../components/PersonalDataForm';
import {createCurriculum, sendCurriculum, downloadBlob, deleteMyAccount, logout} from "./actions";

class PersonalDataContainer extends React.Component{
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit(form)
    {
        this.props.createCurriculum(form);
    }

    uploadCurriculum = data =>
    {
        this.props.sendCurriculum(data);
    };

    handleBlob = data =>
    {
        this.props.downloadBlob()
    };

    deleteMyAccount = event =>
    {
        this.props.deleteMyAccount()
    }

    logout = event =>
    {
        this.props.logout()
    }
    render() {
        var current_user = this.props.current_user;
        return(
            <PersonalDataForm
                current_user={current_user}
                uploadCurriculum={this.uploadCurriculum}
                handleSubmit={this.handleSubmit}
                handleBlob={this.handleBlob}
                deleteMyAccount={this.deleteMyAccount}
                logout={this.logout}
            />
        )
    }

}

function mapStateToProps(state) {
    return { current_user : state.current_user}
};

function mapDispatchToProps(dispatch){
    return bindActionCreators({createCurriculum, sendCurriculum, downloadBlob, deleteMyAccount, logout}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(PersonalDataContainer);
