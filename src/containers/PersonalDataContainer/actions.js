import API from '../../api';
import {setHeaders, getHeaders, logoutSession } from '../../helpers/session';

export function createCurriculum(data){
    const user = {
        user : {data}
    };

    const headers = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        "access-token" : localStorage.getItem("access-token"),
        "client" : localStorage.getItem("client"),
        "uid" : localStorage.getItem("uid")
    };

    const request = API.put('/api/v1/user/1', data, {headers: headers});
    return(dispatch) => {
        request
            .then((response)=>{
                console.log(response.headers);
                setHeaders(response.headers);
                window.Materialize.toast('updated', 4000, 'green');
            })
            .catch((error) => {
                window.Materialize.toast('User or password incorrect', 4000, 'red');
                console.log(error)
            })
    }
}

export function deleteMyAccount(data){
    const user = {
        user : {data}
    };

    const headers = getHeaders();

    const request = API.delete('/auth',  {headers: headers});
    return(dispatch) => {
        request
            .then((response)=>{
                window.location.replace("http://localhost:3001/auth/sign-up")
            })
            .catch((error) => {
                window.Materialize.toast('User or password incorrect', 4000, 'red');
                console.log(error)
            })
    }
}

export function sendCurriculum(data){
    const user = {
        user : {data}
    };

    const headers = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        "access-token" : localStorage.getItem("access-token"),
        "client" : localStorage.getItem("client"),
        "uid" : localStorage.getItem("uid")
    };

    const request = API.put('/api/v1/user/1',  data, {headers: headers});
    return(dispatch) => {
        request
            .then((response)=>{
                setHeaders(response.headers);
                window.Materialize.toast('updated', 4000, 'green');
            })
            .catch((error) => {
                window.Materialize.toast('User or password incorrect', 4000, 'red');
                console.log(error)
            })
    }
}

export function downloadBlob(){

    let url = "http://172.18.9.122:3000/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBDQT09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--80073994f380b430aaceb6469d48f735d3ad2648/Apresentac%CC%A7a%CC%83o1.pdf"
    const request = API.get(url);
    return(dispatch) => {
        request
            .then((response) => response.blob())
            .then((blob) => {
                const url = window.URL.createObjectURL(new Blob([blob]));
                const link = document.createElement('a');
            })
            .catch((error) => {
                window.Materialize.toast('User or password incorrect', 4000, 'red');
                console.log(error)
            })
    }
}

export function logout(data){
    const user = {
        user : {data}
    };

    const headers = getHeaders();

    const request = API.delete('/auth/sign_out',  {headers: headers});
    return(dispatch) => {
        request
            .then((response)=>{
                logoutSession();
                window.location.replace("http://localhost:3001/auth/sign-in")
            })
            .catch((error) => {
                window.Materialize.toast('User or password incorrect', 4000, 'red');
                console.log(error)
            })
    }
}



