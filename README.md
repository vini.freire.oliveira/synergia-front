<h2>Synergia Api: Tutorial step by step to run the react-app</h2>

<h5>System dependencies</h5>

- Docker (https://www.docker.com/)
- Docker-compose

<h5>Step by step to run the project in localhost using docker-compose</h5> 

1. Download the project from wetransfer or git

   ```bash
   git clone https://gitlab.com/vini.freire.oliveira/synergia-front
   ```

2. Open the project folder

3. Build the containers and install the dependecies

   ```dockerfile
   docker-compose build
   ```

4. Now, create database, generate migrations and run the seeds file:

   ```ruby
   docker-compose run --rm web yarn install
   ```

5. Start the server with

   ```
   docker-compose up
   ```

 <h3>Links</h3>

 <h5><a href="https://documenter.getpostman.com/view/110504/S1TU3dnp">Requests document</a></h5>

 <h5><a href="https://gitlab.com/vini.freire.oliveira">My Git</a></h5>

 <h5><a href="https://www.linkedin.com/in/vinicius-freire-b53507107/">My LikedIn</a></h5>

